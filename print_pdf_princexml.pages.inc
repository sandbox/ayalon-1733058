<?php

/**
 * @file
 * Generates the PDF version using princexml
 *
 * This file is included by the print_pdf_princexml module and includes the
 * functions that interface with the princexml library
 *
 * @ingroup print
 */

/**
 * Implements hook_print_pdf_generate().
 */
function print_pdf_princexml_print_pdf_generate($html, $meta, $filename = NULL) {
  $pdf_tool = explode('|', variable_get('print_pdf_pdf_tool', PRINT_PDF_PDF_TOOL_DEFAULT));
  require_once DRUPAL_ROOT . '/' . $pdf_tool[1];

  $print_pdf_prince_binary = variable_get('print_pdf_prince_binary', '');

  // Secure binary path.
  $print_pdf_prince_binary = escapeshellcmd($print_pdf_prince_binary);

  // Load the wrapper class.
  $prince = new Prince($print_pdf_prince_binary);
  $prince->setHTML(TRUE);
  // Enable debugging.
  // $prince->setLog("log.txt");
  header('Pragma: public');
  header('Expires: 0');
  header('Cache-Control: must-revalidate, post-check=0, pre-check=0');
  header('Cache-Control: public');
  header('Content-Description: File Transfer');
  header('Content-Type: application/pdf');
  header('Content-Transfer-Encoding: binary');
  header('Content-Disposition: attachment; filename="' . $filename . '"');
  $prince->convert_string_to_passthru($html);
  return TRUE;
}
