<?php

/**
 * @file
 * Contains the administrative functions of the print_pdf_princexml module.
 *
 * This file is included by the print_pdf_princexml module, and includes the
 * settings form.
 *
 * @ingroup print
 */

/**
 * Form constructor for the princexml options settings form.
 *
 * @ingroup forms
 */
function print_pdf_princexml_settings() {
  $form['settings'] = array(
    '#type' => 'fieldset',
    '#title' => t('princexml options'),
  );

  $form['settings']['print_pdf_prince_binary'] = array(
    '#type' => 'textfield',
    '#title' => t('Path of the prince binary'),
    '#size' => 60,
    '#maxlength' => 250,
    '#default_value' => variable_get('print_pdf_prince_binary', ''),
    '#description' => t('(princexml only) Set the path of the prince executable f.ex. "/usr/bin/prince"'),
  );

  return system_settings_form($form);
}
