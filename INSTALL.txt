1. Download princexml from
http://www.princexml.com/download/

2. Install the prince executable on your server.
Note: If you are using windows, don't install prince in a folder with spaces, it
won't work, use "C:\Prince\" instead. If you have safe_mode enabled, place the
binary in the safe_mode binary folder. 

3. Download php wrapper class from
http://www.princexml.com/download/wrappers/

4. Copy prince.php to sites/all/libraries/prince/prince.php

5. Set the path to the binary in the PDF settings
under admin/config/user-interface/print/pdf/princexml

6. All settings to the pdf output can be made via CSS Style sheet. See f.ex.
http://www.princexml.com/doc/7.0/page-size/

All settings f.ex. orientation on the PDF configuration page will be ignored.
